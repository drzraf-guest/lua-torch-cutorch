Source: lua-torch-cutorch
Section: contrib/science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Mo Zhou <cdluminate@gmail.com>
Build-Depends: debhelper (>= 11~),
               cmake,
               dh-lua,
               luajit,
               gcc-6,
               g++-6,
               nvidia-cuda-toolkit,
               lua-torch-torch7,
               libtorch-th-dev,
               libtorch-luat-dev,
Standards-Version: 4.1.4
Homepage: https://github.com/torch/cutorch
Vcs-Git: https://salsa.debian.org/science-team/lua-torch-cutorch.git
Vcs-Browser: https://salsa.debian.org/science-team/lua-torch-cutorch

Package: lua-torch-cutorch
Architecture: amd64 ppc64el
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends},
         luajit | lua5.1,
         lua-torch-torch7-dev,
         libtorch-th-dev,
         libtorch-luat-dev,
         libtorch-thc (= ${binary:Version}),
XB-Lua-Versions: ${lua:Versions}
Description: CUDA backend for Torch framework
 Cutorch provides a CUDA backend for torch7. It provides the following:
 .
  - a new tensor type: `torch.CudaTensor` that acts like `torch.FloatTensor`,
    but all it's operations are on the GPU. Most of the tensor operations are
    supported by cutorch. There are a few missing ones, which are being
    implemented. The missing list can be found here:
      https://github.com/torch/cutorch/issues/70
  - several other GPU tensor types, with limited functionality. Currently
    limited to copying/conversion, and several indexing and shaping operations.
  - `cutorch.*` - Functions to set/get GPU, get device properties, memory
    usage, set/get low-level streams, set/get random number generator's seed,
    synchronization etc. They are described in more detail below.
 .
 This package ships the Lua interface of Cutorch.

Package: libtorch-thc
Architecture: amd64 ppc64el
Section: contrib/libs
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends},
Recommends: libtorch-thc-dev (= ${binary:Version})
Description: libTHC.so for Torch framework (library)
 Cutorch provides a CUDA backend for torch7. It provides the following:
 .
  - a new tensor type: `torch.CudaTensor` that acts like `torch.FloatTensor`,
    but all it's operations are on the GPU. Most of the tensor operations are
    supported by cutorch. There are a few missing ones, which are being
    implemented. The missing list can be found here:
      https://github.com/torch/cutorch/issues/70
  - several other GPU tensor types, with limited functionality. Currently
    limited to copying/conversion, and several indexing and shaping operations.
  - `cutorch.*` - Functions to set/get GPU, get device properties, memory
    usage, set/get low-level streams, set/get random number generator's seed,
    synchronization etc. They are described in more detail below.
 .
 This package ships the libTHC.so library.

Package: libtorch-thc-dev
Architecture: amd64 ppc64el
Section: contrib/libdevel
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends},
         libtorch-thc (= ${binary:Version}),
Description: libTHC.so for Torch framework (development)
 Cutorch provides a CUDA backend for torch7. It provides the following:
 .
  - a new tensor type: `torch.CudaTensor` that acts like `torch.FloatTensor`,
    but all it's operations are on the GPU. Most of the tensor operations are
    supported by cutorch. There are a few missing ones, which are being
    implemented. The missing list can be found here:
      https://github.com/torch/cutorch/issues/70
  - several other GPU tensor types, with limited functionality. Currently
    limited to copying/conversion, and several indexing and shaping operations.
  - `cutorch.*` - Functions to set/get GPU, get device properties, memory
    usage, set/get low-level streams, set/get random number generator's seed,
    synchronization etc. They are described in more detail below.
 .
 This package ships the development files for libTHC.so .
